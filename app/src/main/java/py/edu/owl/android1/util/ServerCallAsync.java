package py.edu.owl.android1.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import android.os.AsyncTask;
import android.util.Log;


/**
 * @author dcerrano<cerrano.diego@gmail.com>
 * @since 1.0 27/11/15
 */
public class ServerCallAsync extends AsyncTask<String, Void, String> {
    private static final String TAG = ServerCallAsync.class.getCanonicalName();
    @Override
    protected String doInBackground(String... urls) {
        String urlString = urls[0];
        StringBuffer response = new StringBuffer("");
        try {
            Log.d(TAG, "INVOCANDO URL: " + urlString);
            URL url = new URL(urlString);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));

            String line = "";
            while ((line = buffer.readLine()) != null) {
                response.append(line);
            }
            connection.disconnect();
        } catch (Exception exc) {
            Log.e(TAG, "Error:" + exc);
        }
        String responseStr = response.toString();
        Log.d(TAG, "Respuesta: " + responseStr);
        return responseStr;
    }
}

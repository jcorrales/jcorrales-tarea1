package py.edu.owl.android1.service;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import py.edu.owl.android1.modelo.Categoria;
import py.edu.owl.android1.util.AsyncCallback;
import py.edu.owl.android1.util.ServerCallAsync;

/**
 * Created by dcerrano on 27/11/15.
 */
public class CategoriaService {

    private static final String TAG = CategoriaService.class.getCanonicalName();


    public void list(final AsyncCallback<List<Categoria>> asyncCallback) {

        ServerCallAsync server = new ServerCallAsync() {
            @Override
            protected void onPostExecute(String result) {
                try {
                    if (result == null || result.compareTo("") == 0) {
                        throw new Exception("No se obtuvo respuesta del servidor");
                    }
                    List<Categoria> lista = getCategoriaListFromJson(result);
                    asyncCallback.onComplete(true, lista, null);
                }catch(JSONException jexc) {
                    Log.e(TAG, jexc.getMessage());
                    Exception error = new Exception("No se pudo procesar los datos de categorias");
                    asyncCallback.onComplete(false, null, error);
                    jexc.printStackTrace();
                } catch (Exception exc) {
                    asyncCallback.onComplete(false, null, exc);
                }
            }
        };
        server.execute(" http://django-owlinstitute.rhcloud.com/categorias/?format=json");
    }

    private List<Categoria> getCategoriaListFromJson(String response)
            throws JSONException {
        Log.i(TAG, "Procesando lista de categorias");
        List<Categoria> categorias = new ArrayList<>();

        JSONArray categoriasArray = new JSONArray(response);

        int len = categoriasArray.length();
        for (int i = 0; i < len; i++) {
            JSONObject categoriaJO = categoriasArray.getJSONObject(i);

            String codigo = categoriaJO.getString("codigo");
            String nombre = categoriaJO.getString("nombre");
            Integer id = categoriaJO.getInt("id");
            Categoria categoria = new Categoria(id, codigo, nombre);
            categorias.add(categoria);
        }
        return categorias;
    }

}

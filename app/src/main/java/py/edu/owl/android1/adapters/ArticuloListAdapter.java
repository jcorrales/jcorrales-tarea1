package py.edu.owl.android1.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import py.edu.owl.android1.R;
import py.edu.owl.android1.modelo.Articulo;
import py.edu.owl.android1.util.DownloadImageTaskAsync;

public class ArticuloListAdapter extends ListAdapterAncestor<Articulo> {
    private Map<String, Bitmap> logos = new HashMap<>();

    public ArticuloListAdapter(Context context, List<Articulo> articuloList) {
        super(context, R.layout.articulo_list_item, articuloList);
    }

    @Override
    protected void dataToLayout(View row, final Articulo articulo, int position) {

        final ImageView iconIv = (ImageView) row.findViewById(R.id.articulo_list_item_image);

        Bitmap bitmap = logos.get(articulo.getCodigo());
        /*Almacenamos los fotos para no descargar varias veces*/
        if (bitmap != null) {
            iconIv.setImageBitmap(bitmap);
        } else {

            DownloadImageTaskAsync imgAsync = new DownloadImageTaskAsync() {
                @Override
                protected void onPostExecute(Bitmap result) {
                    if (result != null) {
                        iconIv.setImageBitmap(result);
                        logos.put(articulo.getCodigo(), result);
                    }
                }
            };
            imgAsync.execute(articulo.getFoto());
        }

        TextView nombreTv = (TextView) row.findViewById(R.id.articulo_list_item_nombre);
        nombreTv.setText(articulo.getNombre());

        TextView precioTv = (TextView) row.findViewById(R.id.articulo_list_item_precio);
        precioTv.setText(articulo.getPrecio());
    }
}

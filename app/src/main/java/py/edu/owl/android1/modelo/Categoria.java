package py.edu.owl.android1.modelo;

/**
 * Created by juan on 03/01/16.
 */
public class Categoria {
    private Integer id;
    private String codigo;
    private String nombre;

    public Categoria() {
    }

    public Categoria(Integer id, String codigo, String nombre) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}

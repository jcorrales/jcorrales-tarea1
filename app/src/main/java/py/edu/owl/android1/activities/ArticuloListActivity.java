package py.edu.owl.android1.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import py.edu.owl.android1.R;
import py.edu.owl.android1.adapters.ArticuloListAdapter;
import py.edu.owl.android1.modelo.Articulo;
import py.edu.owl.android1.service.ArticuloService;
import py.edu.owl.android1.util.AsyncCallback;

public class ArticuloListActivity extends Activity {

    private ArticuloService articuloService = new ArticuloService();
    private static final String TAG = ArticuloListActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articulo_list);

        final Integer idCategoria = getIntent().getIntExtra("id", -1);

        AsyncCallback<List<Articulo>> asyncCallback =
                new AsyncCallback<List<Articulo>>() {
                    @Override
                    public void onComplete(boolean success,
                                           List<Articulo> result,
                                           Throwable caught) {
                        if (success) {
                            if(result.size() > 0){
                                mostrarArticulos(result);
                            }
                            else{
                                Toast.makeText(getApplicationContext(),
                                        "No se encontraron articulos para la categoria "+idCategoria,
                                        Toast.LENGTH_LONG)
                                        .show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + caught.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                };
        articuloService.list(asyncCallback, idCategoria);
    }

    private void mostrarArticulos(final List<Articulo> articuloList) {

        ListView articulosListView = (ListView)
                findViewById(R.id.lista_articulos);

        articulosListView.setAdapter(
                new ArticuloListAdapter(ArticuloListActivity.this, articuloList));

        articulosListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Articulo articulo = articuloList.get(position);

                Intent intent = new Intent(ArticuloListActivity.this,
                        ArticuloDetalleActivity.class);
                intent.putExtra("id", articulo.getId());


                startActivity(intent);
            }
        });
    }
}

package py.edu.owl.android1.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import py.edu.owl.android1.R;
import py.edu.owl.android1.adapters.CategoriaListAdapter;
import py.edu.owl.android1.modelo.Categoria;
import py.edu.owl.android1.service.CategoriaService;
import py.edu.owl.android1.util.AsyncCallback;

public class CategoriaListActivity extends Activity {

    private CategoriaService categoriaService = new CategoriaService();
    private static final String TAG = CategoriaListActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categoria_list);

        AsyncCallback<List<Categoria>> asyncCallback =
                new AsyncCallback<List<Categoria>>() {
                    @Override
                    public void onComplete(boolean success,
                                           List<Categoria> result,
                                           Throwable caught) {
                        if (success) {
                            mostrarCategorias(result);
                        } else {
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + caught.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                };

        categoriaService.list(asyncCallback);
    }

    private void mostrarCategorias(final List<Categoria> categoriaList) {

        ListView categoriasListView = (ListView)
                findViewById(R.id.lista_categorias);
        categoriasListView.setAdapter(
                new CategoriaListAdapter(CategoriaListActivity.this, categoriaList));

        categoriasListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Categoria categoria = categoriaList.get(position);
                Intent intent = new Intent(CategoriaListActivity.this,
                        ArticuloListActivity.class);
                intent.putExtra("id", categoria.getId());

                startActivity(intent);
            }
        });
    }
}

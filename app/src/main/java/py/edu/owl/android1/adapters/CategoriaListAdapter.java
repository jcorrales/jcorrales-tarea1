package py.edu.owl.android1.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import py.edu.owl.android1.R;
import py.edu.owl.android1.modelo.Categoria;

public class CategoriaListAdapter extends ListAdapterAncestor<Categoria> {
    private Map<String, Bitmap> logos = new HashMap<>();

    public CategoriaListAdapter(Context context, List<Categoria> categoriaList) {
        super(context, R.layout.categoria_list_item, categoriaList);
    }

    @Override
    protected void dataToLayout(View row, final Categoria categoria, int position) {

        TextView codigoTv = (TextView) row.findViewById(R.id.categoria_list_item_codigo);
        codigoTv.setText(categoria.getCodigo());

        TextView nombre = (TextView) row.findViewById(R.id.categoria_list_item_nombre);
        nombre.setText(categoria.getNombre());
    }
}

package py.edu.owl.android1.util;

import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * @author dcerrano<cerrano.diego@gmail.com>
 * @since 1.0 27/11/15
 */
public abstract class ServerCallAsyncAncestor<Result> extends AsyncTask<String, Void, Result> {

    private static final String TAG = ServerCallAsyncAncestor.class.getCanonicalName();

    @Override
    protected Result doInBackground(String... urls) {
        String urlString = urls[0];
        Result result = null;
        try {
            Log.d(TAG, "INVOCANDO URL: " + urlString);
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            result = getResult(inputStream);
            connection.disconnect();
        } catch (Exception exc) {
            Log.e(TAG, "Error:" + exc);
        }

        return result;
    }

    protected abstract Result getResult(InputStream inputStream);
}

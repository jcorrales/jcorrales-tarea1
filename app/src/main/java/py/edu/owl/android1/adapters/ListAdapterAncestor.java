package py.edu.owl.android1.adapters;


import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import py.edu.owl.android1.R;
import py.edu.owl.android1.modelo.ListItem;

public class ListAdapterAncestor<T> extends ArrayAdapter<T> {
    private final LayoutInflater inflater;
    private final List<T> objects;
    private int rowLayoutResource;
    protected  int listSize;

    public ListAdapterAncestor(Context context, @LayoutRes int rowLayoutResource, List<T> objects) {

        super(context, rowLayoutResource, objects);
        inflater = ((Activity) context).getLayoutInflater();
        this.objects = objects;
        this.rowLayoutResource =rowLayoutResource;
        if(objects!= null) {
        listSize = objects.size();
        }else {
            listSize = 0;
        }
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return getCustomView(position, convertView, parent);
        } else {
            return super.getDropDownView(position, convertView, parent);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        View row;

        if (convertView == null) {
            row = inflater.inflate(rowLayoutResource, null);
        } else {
            row = convertView;
        }

        dataToLayout(row, getItem(position), position);
        return row;
    }

    protected void dataToLayout( View row, T item, int position) {

    }
}
package py.edu.owl.android1.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import py.edu.owl.android1.R;
import py.edu.owl.android1.modelo.Articulo;
import py.edu.owl.android1.service.ArticuloService;
import py.edu.owl.android1.util.AsyncCallback;
import py.edu.owl.android1.util.DownloadImageTaskAsync;

public class ArticuloDetalleActivity extends Activity {

    private ArticuloService articuloService = new ArticuloService();
    private static final String TAG = ArticuloDetalleActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Integer idArticulo = getIntent().getIntExtra("id", -1);

        AsyncCallback<Articulo> asyncCallback =
                new AsyncCallback<Articulo>() {
                    @Override
                    public void onComplete(boolean success, Articulo result,
                                           Throwable caught) {
                        if (success) {
                            mostrarArticulo(result);
                        } else {
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + caught.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                };

        articuloService.obtenerDetalles(asyncCallback, idArticulo);
    }

    private void mostrarArticulo(final Articulo articulo) {
        setContentView(R.layout.activity_articulo_detalle);

        final ImageView iconIv = (ImageView) findViewById(R.id.articulo_detalle_foto);

        DownloadImageTaskAsync imgAsync = new DownloadImageTaskAsync() {
            @Override
            protected void onPostExecute(Bitmap result) {
                if (result != null) {
                    iconIv.setImageBitmap(result);
                }
            }
        };

        imgAsync.execute(articulo.getFoto());

        TextView nombreTv =
                (TextView) findViewById(R.id.articulo_detalle_nombre);
        nombreTv.setText(articulo.getNombre());

        TextView codigoTv =
                (TextView) findViewById(R.id.articulo_detalle_codigo);
        codigoTv.setText(articulo.getCodigo());

        TextView descripcionTv =
                (TextView) findViewById(R.id.articulo_detalle_descripcion);
        descripcionTv.setText(articulo.getDescripcion());

        TextView precioTv =
                (TextView) findViewById(R.id.articulo_detalle_precio);
        precioTv.setText(articulo.getPrecio());
    }
}

package py.edu.owl.android1.service;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import py.edu.owl.android1.modelo.Articulo;
import py.edu.owl.android1.util.AsyncCallback;
import py.edu.owl.android1.util.ServerCallAsync;

/**
 * Created by dcerrano on 27/11/15.
 */
public class ArticuloService {

    private static final String TAG = ArticuloService.class.getCanonicalName();


    public void list(final AsyncCallback<List<Articulo>> asyncCallback) {

        ServerCallAsync server = new ServerCallAsync() {
            @Override
            protected void onPostExecute(String result) {
                try {
                    if (result == null || result.compareTo("") == 0) {
                        throw new Exception("No se obtuvo respuesta del servidor");
                    }
                    List<Articulo> lista = getArticuloListFromJson(result);
                    asyncCallback.onComplete(true, lista, null);
                }catch(JSONException jexc) {
                    Log.e(TAG, jexc.getMessage());
                    Exception error = new Exception("No se pudo procesar datos de articulos");
                    asyncCallback.onComplete(false, null, error);
                    jexc.printStackTrace();
                } catch (Exception exc) {
                    asyncCallback.onComplete(false, null, exc);
                }
            }
        };
        server.execute("http://django-owlinstitute.rhcloud.com/articulos/?format=json");
    }

    private List<Articulo> getArticuloListFromJson(String response)
            throws JSONException {
        Log.i(TAG, "Procesando lista de articulos");
        List<Articulo> articulos = new ArrayList<>();

        JSONArray articulosArray = new JSONArray(response);

        int len = articulosArray.length();
        for (int i = 0; i < len; i++) {
            JSONObject articuloJO = articulosArray.getJSONObject(i);

            Integer id = articuloJO.getInt("id");
            String codigo = articuloJO.getString("codigo");
            String nombre = articuloJO.getString("nombre");
            String descripcion = articuloJO.getString("descripcion");
            String precio = articuloJO.getString("precio");
            String foto = articuloJO.getString("foto");
            Articulo articulo = new Articulo(id, codigo, nombre, descripcion, precio, foto);
            articulos.add(articulo);
        }
        return articulos;
    }

    public void obtenerDetalles(final AsyncCallback<Articulo> asyncCallback, Integer idArticulo){
        ServerCallAsync serverCallAsync = new ServerCallAsync(){
            @Override
            protected void onPostExecute(String result) {
                Articulo articulo = null;
                try {
                    if (result == null || result.compareTo("") == 0) {
                        throw new Exception("No se obtuvo respuesta del servidor");
                    }
                    articulo = getArticuloFromJson(result);
                    asyncCallback.onComplete(true, articulo, null);
                }catch(JSONException jexc) {
                    Log.e(TAG, jexc.getMessage());
                    Exception error = new Exception("No se pudo procesar datos de articulo ");
                    asyncCallback.onComplete(false, null, error);
                    jexc.printStackTrace();
                } catch (Exception exc) {
                    asyncCallback.onComplete(false, null, exc);
                }
            }
        };
        serverCallAsync.execute("http://django-owlinstitute.rhcloud.com/articulos/"+idArticulo.intValue()+"/?format=json");
    }

    private Articulo getArticuloFromJson(String result) throws JSONException {
        Log.i(TAG, "Procesando detalle de articulo");
        Articulo articulo = new Articulo();

        JSONObject articuloJO = new JSONObject(result);

            Integer id = articuloJO.getInt("id");
            String codigo = articuloJO.getString("codigo");
            String nombre = articuloJO.getString("nombre");
            String descripcion = articuloJO.getString("descripcion");
            String precio = articuloJO.getString("precio");
            String foto = articuloJO.getString("foto");

            articulo = new Articulo(id, codigo, nombre, descripcion, precio, foto);
        return articulo;
    }

    public void list(final AsyncCallback<List<Articulo>> asyncCallback, Integer idCategoria) {

        ServerCallAsync server = new ServerCallAsync() {
            @Override
            protected void onPostExecute(String result) {
                try {
                    if (result == null || result.compareTo("") == 0) {
                        throw new Exception("No se obtuvo respuesta del servidor");
                    }
                    List<Articulo> lista = getArticuloListFromJson(result);
                    asyncCallback.onComplete(true, lista, null);
                }catch(JSONException jexc) {
                    Log.e(TAG, jexc.getMessage());
                    Exception error = new Exception("No se pudo procesar datos de articulos");
                    asyncCallback.onComplete(false, null, error);
                    jexc.printStackTrace();
                } catch (Exception exc) {
                    asyncCallback.onComplete(false, null, exc);
                }
            }
        };
        server.execute("http://django-owlinstitute.rhcloud.com/articulos/?categoria="+idCategoria.intValue()+"&format=json");
    }
}
